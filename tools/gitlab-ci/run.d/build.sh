
citbx_use "dockerimg"

job_main() {
    BUILD_DOCKER_TYPE=${CI_JOB_NAME##*-}
    if [ "${CI_COMMIT_TAG##*-}" != "$BUILD_DOCKER_TYPE" ]; then
        print_critical "The tag suffix '${CI_COMMIT_TAG##*-}' mismatch with the job suffix '$BUILD_DOCKER_TYPE'" \
                        "This error is probably due to:" \
                        " * on a Gitlab runner: Missing or incorrect 'only' tag in the .gitlab-ci.yml - You can put this one:" \
                        "    only:" \
                        "        - /^.*-$BUILD_DOCKER_TYPE\$/" \
                        " * on your local worspace: You have launched ci-toolbox $CI_JOB_NAME with the wrong tag - Try with the additional option:" \
                        "    --image-tag <docker_version>-$BUILD_DOCKER_TYPE"
    fi
    echo "FROM docker:$CI_COMMIT_TAG" > Dockerfile
    case "$BUILD_DOCKER_TYPE" in
        dind)
            cat entrypoint.d/add-ca-certificates.sh > entrypoint-insert-content.sh
            cat Dockerfile.d/add-ca-certs >> Dockerfile
            ;;
        git)
            cat entrypoint.d/add-ca-certificates.sh \
                entrypoint.d/add-docker-group.sh > entrypoint-insert-content.sh
            cat Dockerfile.d/add-packages >> Dockerfile
            ;;
        *)
            print_critical "Unsupported docker image (type='$BUILD_DOCKER_TYPE')"
            ;;
    esac
    sed 's/dockerd-entrypoint/docker-entrypoint/g' Dockerfile.d/add-ca-certs >> Dockerfile
    docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG .
    if [ -n "$CI_BUILD_TOKEN" ]; then
        docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
        docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    fi
}

job_after() {
    local retcode=$1
    rm -f Dockerfile entrypoint-insert-content.sh
    if [ $retcode -eq 0 ]; then
        print_info "Image \"$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG\" successfully generated"
    fi
}
