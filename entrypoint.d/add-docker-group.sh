
# Add a docker group with a specific GID
if [ -n "$DOCKER_GID" ]; then
    # Same as `addgroup -g $DOCKER_GID docker` ignoring duplicate GID
    addgroup -S docker
    sed -E -i 's/^docker:x:\d+:/docker:x:'"$DOCKER_GID"':/g' /etc/group
fi
