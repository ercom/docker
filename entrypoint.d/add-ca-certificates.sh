
write_ca_certs() {
    local dir=$1
    mkdir -p "$dir"
    (echo "$CI_SERVER_TLS_CA_FILE" \
        && echo "$PROJECT_ADD_TLS_CA_FILE") | awk '
        split_after == 1 {
            n++;
            split_after=0;
        }
        /-----END CERTIFICATE-----/ {
            split_after=1;
        }
        {
            print > "'"$dir"'/custom-ca-cert-" n ".crt"
        }'
}

if [ -n "$CI_SERVER_TLS_CA_FILE" ] || [ -n "$PROJECT_ADD_TLS_CA_FILE" ]; then
    write_ca_certs /usr/local/share/ca-certificates/
    if ! update-ca-certificates 2>> /var/log/update-ca-certificates.log ; then
        echo "update-ca-certificates failed!"
        cat /var/log/update-ca-certificates.log
    fi
    mkdir -p /etc/docker/certs.d
    cp /usr/local/share/ca-certificates/*.crt /etc/docker/certs.d/
fi
